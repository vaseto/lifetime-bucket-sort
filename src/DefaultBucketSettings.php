<?php

/**
    Display % from all users being subscribed for < 1 day
    Display % from all users being subscribed for 1-3 days
    Display % from all users being subscribed for 3-7 days
    Display % from all users being subscribed for 7-14 days
    Display % from all users being subscribed for 14-21 days
    Display % from all users being subscribed for 21-28 days
    Display % from all users being subscribed for >28 days
*/
return [
    1 => [
        'lower' => 0,
        'higher' => 1,
        'label' => '< 1 day',
    ],
    2 => [
        'lower' => 1,
        'higher' => 3,
        'label' => '1-3 days',
    ],
    3 => [
        'lower' => 3,
        'higher' => 7,
        'label' => '3-7 days',
    ],
    4 => [
        'lower' => 7,
        'higher' => 14,
        'label' => '7-14 days',
    ],
    5 => [
        'lower' => 14,
        'higher' => 21,
        'label' => '14-21 days',
    ],
    6 => [
        'lower' => 21,
        'higher' => 28,
        'label' => '21-28 days',
    ],
    7 => [
        'lower' => 28,
        'higher' => PHP_INT_MAX,
        'label' => '> 28 days',
    ],
];

